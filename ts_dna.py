import requests
import json

controller = "devnetapi.cisco.com/sandbox/apic_em"

def getTicket():
    url = "https://"+controller+"/api/v1/ticket"
    payload = {"username":"devnetuser","password":"Cisco123!"}
    header = {'content-type': 'application/json'}

    response = requests.post(url, data = json.dumps(payload), headers=header,verify=False)

    print(response)
    r_json = response.json()

    tiket = r_json["response"]["serviceTicket"]
    return tiket

def getNetworkDevices(ticket):
    url = "https://" + controller + "/api/v1/network-device"
    header = {"content-type":"application/json","X-Auth-Token":ticket}

    response = requests.get(url, headers=header,verify=False)

    print("Network Devices = ")
    print(json.dumps(response.json(),indent=4,separators=(',',': ')))
    r_json = response.json()

    for i in r_json["response"]:
        print(i["id"] + "   " + '{:53}'.format(i["series"]) + "  " + i["reachabilityStatus"])

theTicket=getTicket()
getNetworkDevices(theTicket)